import React from "react";
import "./App.css";
import UserContainer from "./components/UserContainer";
import { Provider } from "react-redux";
import store from "./redux/store";
import SideNavbar from "./components/SideNavbar";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Home from "./pages/Home";
import Reports from "./pages/Reports";
import Products from "./pages/Products";
import About from "./pages/about";
import Services from "./pages/services";
import Contact from "./pages/contact";
import SignUp from "./pages/signup";
import Navbar from "./components/Index";

function App() {
  return (
    <Provider store={store}>
      <Router>
        <SideNavbar />

        <Switch>
          <Route path="/" exact component={Home}></Route>
          <Route path="/reports" component={UserContainer}></Route>
          <Route path="/products" component={Products}></Route>
          <Route path="/about" component={About} />
          <Route path="/services" component={Services} />
          <Route path="/contact-us" component={Contact} />
          <Route path="/sign-up" component={SignUp} />
        </Switch>
      </Router>
      <Router>{/* <Navbar /> */}</Router>
    </Provider>
  );
}

export default App;
