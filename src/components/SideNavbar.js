import React, { useState } from "react";
import * as FaIcons from "react-icons/fa";
import * as AiIcons from "react-icons/ai";
import { Link } from "react-router-dom";
import { SidebarData } from "./SidebarData";
import "./SideNavbar.css";
import { IconContext } from "react-icons";

function SideNavbar() {
  const [sidebar, setSideBar] = useState(false);

  const showSidebar = () => setSideBar(!sidebar);
  return (
    <div>
      <IconContext.Provider value={{ color: "#fff" }}>
        <div className="navbar">
          <Link to="#" className="menu-bars">
            <FaIcons.FaBars onClick={showSidebar} />
          </Link>

          <div className="top_Navbar">
            <Link to="">
              <h1 className="topnav">Home</h1>
            </Link>
            <Link to="/contact-us">
              <h1 className="topnav">Contact</h1>
            </Link>
            <Link to="/services">
              <h1 className="topnav">Services</h1>
            </Link>
            <Link to="/about">
              <h1 className="topnav">AboutUs</h1>
            </Link>
            <Link to="/sign-up">
              <h1 className="topnav">SignUp</h1>
            </Link>
          </div>
        </div>
        <nav className={sidebar ? "nav-menu active" : "nav-menu"}>
          <ul className="nav-menu-items" onClick={showSidebar}>
            <li className="navbar-toggle">
              <Link to="#" className="menu-bars">
                <AiIcons.AiOutlineClose />
              </Link>
            </li>
            {SidebarData.map((item, index) => {
              return (
                <li key={index} className={item.cName}>
                  <Link to={item.path}>
                    {item.icon}
                    <span>{item.title}</span>
                  </Link>
                </li>
              );
            })}
          </ul>
        </nav>
      </IconContext.Provider>
    </div>
  );
}

export default SideNavbar;
